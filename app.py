from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
app = Flask(__name__)

ENV = 'dev'

if ENV == 'dev':
    app.debug = True
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@localhost/simple_flask'
else:
    app.debug = False
    app.config['SQLALCHEMY_DATABASE_URI'] = ''

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)


class Feedback(db.Model):
    __tablename__ = 'feedback'
    id = db.Column(db.Integer, primary_key=True)
    writer = db.Column(db.String(200), unique=True)
    message = db.Column(db.String(200))
    count = db.Column(db.Integer)

    def __init__(self, writer, message, count):
        self.writer = writer
        self.message = message
        self.count = count


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/submit', methods=['POST'])
def submit():
    if request.method == 'POST':
        writer = request.form['username']
        message = request.form['message']
        # print(writer, message)
        if writer == '' or message == '':
            return render_template('index.html', message='Please enter data')
        if db.session.query(Feedback).filter(Feedback.writer == writer).count() == 0:
            data = Feedback(writer, message)
            db.session.add(data)
            db.session.commit()
            return render_template('success.html')
        return render_template('index.html', message='This was written')


if __name__ == "__main__":
    app.run()
